import generateRandomId from '../../support/utilities';

/**
 * @author Ben Hayward
 * @create date 2019-08-09 14:42:51
 * @desc Spec tests for comment threads.
 */
context('Comment Threads', () => {
  const postText = generateRandomId();
  const commentPostButton = '.m-commentPoster__postButton';
  const textArea = 'minds-textarea';
  const commentBlock = '.minds-block';
  const viewMoreButton = '.m-comments__viewMore button';
  const messageText = '.m-comment__message';
  const optimisticEntity = '.m-commentsEntityOutlet__optimisticList';
  const commentBody = '.minds-body';
  const thread = 'm-comments__thread';
  const repliesToggle = '.m-comment__repliesToggle span';

  before(() => {
    //make a post new.
    cy.getCookie('minds_sess').then((sessionCookie) => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });

    // This test makes use of cy.post()
    cy.post(postText);
  });

  beforeEach(() => {
    cy.preserveCookies();
    cy.intercept('POST', '**/api/v1/comments/**').as('postComment');
    cy.rateLimitBypass();
  });

  it.skip('should post three tiers of comments', () => {
    // within of post body
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within(($list) => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        // type comment
        cy.get(textArea).type('test 1');

        // hit post button.
        cy.get(commentPostButton).click().wait('@POSTComment');

        // within new nested comment block (t2)
        cy.get(commentBlock).within(($list) => {
          // click reply
          cy.contains('Reply').click();

          // type comment and post
          cy.get(textArea).type('test 2');
          cy.get(commentPostButton).click().wait('@POSTComment');

          // within new nested comment block (t3)
          cy.get(commentBlock).within(($list) => {
            // click reply
            cy.contains('Reply').click();

            // type comment and post
            cy.get(textArea).type('test 3');
            cy.get(commentPostButton).click().wait('@POSTComment');
          });
        });
      });
  });

  // i.e. newest comments on top
  it.skip('should display each tier in reverse order', () => {
    // within of post body
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within(($list) => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        // --------------------
        // TOP TIER

        // type first L0 comment and post
        cy.get(textArea).first().type('test 1a');
        cy.get(commentPostButton).first().click().wait('@POSTComment');

        // type second L0 comment and post
        cy.get(textArea).first().type('test 1b');
        cy.get(commentPostButton).first().click().wait('@POSTComment');

        // --------------------
        // SECOND TIER
        // within new nested comment block (t2)

        cy.get(commentBlock)
          .first()
          .within(($list) => {
            // click reply
            cy.contains('Reply').click();

            // type comment and post
            cy.get(textArea).type('test 2a');
            cy.get(commentPostButton).click().wait('@POSTComment');

            // type comment and post
            cy.get(textArea).type('test 2b');
            cy.get(commentPostButton).click().wait('@POSTComment');
          });

        // Check the first comment is displayed before the second one
        cy.get(optimisticEntity)
          .first()
          .within(($list) => {
            cy.get(commentBody).within(($list2) => {
              cy.get(messageText).within(($list3) => {
                cy.contains('test 1a');
              });
            });
          });

        // Check the first reply is displayed before the second one
        cy.get(thread)
          .first()
          .within(($list) => {
            cy.get(commentBlock)
              .first()
              .within(($list2) => {
                cy.get(messageText).within(($list3) => {
                  cy.contains('test 2a');
                });
              });
          });
      });
  });

  it.skip('should paginate correctly', () => {
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within(($list) => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        for (let i = 0; i < 25; i++) {
          cy.get(textArea).last().type(`comment n°${i}`);
          cy.get(commentPostButton).last().click().wait('@POSTComment');
        }

        cy.get('m-relativetimespan').first().click();
      });

    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within(($list) => {
        cy.get(commentBlock)
          .first()
          .within(($list) => {
            cy.intercept('GET', '**/api/v2/comments/**').as('GETcomments');
            cy.get(repliesToggle).click().wait('@GETcomments');
            cy.get(viewMoreButton).click().wait('@GETcomments');
          });
      });
  });
});
