context('Rewards Product Page', () => {
  const buyButton = '[data-cy=data-minds-join-rewards-button] button div';

  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (!sessionCookie) {
        return cy.login(true);
      }
    });
  });

  beforeEach(() => {
    cy.preserveCookies();
  });

  it('should have a join rewards button', () => {
    cy.visit('/rewards');

    cy.get(buyButton)
      .should('be.visible')
      .contains('Buy Tokens')
      .click();

    cy.get('.m-web3Modal').should('be.visible');
  });
});
