import generateRandomId from '../support/utilities';

context('Registration', () => {
  var username = generateRandomId();
  const usernameInUse = Cypress.env().username;
  const password = `${generateRandomId()}0oA!`;
  const email = 'test@minds.com';
  const invalidPassword = 'abcd';

  const usernameField = 'm-registerForm #username';
  const emailField = 'm-registerForm #email';
  const passwordField = 'm-registerForm #password';
  const password2Field = 'm-registerForm #password2';
  const checkbox = '[data-cy=minds-accept-tos-input] [type=checkbox]';
  const submitButton = '[type="submit"]';
  const mindsAvatar = 'minds-avatar';
  const registerFieldError = '.m-register__error';
  const registerFormError = '.m-register__formError';

  beforeEach(() => {
    cy.clearCookies();
    cy.bypassFriendlyCaptcha();

    cy.visit('/register');
    cy.location('pathname').should('eq', '/register');
    cy.intercept('POST', '**/api/v1/register').as('register');
  });

  // Commenting out until successful registration test is fixed.
  // after(() => {
  //   cy.visit('/login');
  //   cy.location('pathname').should('eq', '/login');
  //   cy.login(false, username, password);
  //   cy.deleteUser(username, password);
  //   cy.clearCookies();
  // });

  it('should not enable "Join Now" button if password is empty', () => {
    cy.get(usernameField).focus().type(username);

    cy.get(emailField).focus().type(email);

    cy.get(submitButton).should('be.disabled');
  });

  it('should not enable "Join Now" button if Terms & Conditions not accepted', () => {
    cy.get(usernameField).focus().type(username);

    cy.get(emailField).focus().type(email);

    cy.get(passwordField).focus().type(password);

    cy.wait(500);

    cy.get(password2Field).focus().type(password);

    cy.get(submitButton).should('be.disabled');
  });

  it('should display an error if user is already registered', () => {
    cy.get(usernameField).focus().type(usernameInUse);

    cy.get(registerFieldError).contains('Username already taken.');
  });

  it('should display an error if password is invalid', () => {
    cy.get(passwordField).focus().type(invalidPassword);

    cy.wait(500);

    cy.get(password2Field).focus().type(invalidPassword);

    cy.get(registerFieldError).contains('Invalid password.');
  });

  it('should display an error if passwords do not match', () => {
    cy.get(passwordField).focus().type(password);

    cy.wait(500);

    cy.get(password2Field)
      .focus()
      .type(password + '!');

    cy.get(registerFieldError).contains('Passwords must match');
  });

  // Old CAPTCHA system.
  it.skip('should not enable "Join Now" button if Captcha not accepted', () => {
    username = generateRandomId();

    cy.get(usernameField).focus().type(username);

    cy.get(emailField).focus().type(email);

    cy.get(passwordField).focus().type(password);

    cy.wait(500);

    cy.get(password2Field).focus().type(password);

    cy.get(checkbox).click({ force: true });

    cy.get(submitButton)
      .contains('Join Now')
      .click()
      .wait('@register')
      .then((xhr) => {
        expect(xhr.response.statusCode).to.equal(200);
      });

    cy.get(registerFormError).contains('Captcha failed');
  });

  it('should set correct metadata when a referrer param is NOT present', () => {
    cy.visit('/register');

    cy.get('head meta[property="og:image"]')
      .should('have.attr', 'content')
      .and('match', /default-v3.png/);

    cy.get('head meta[property="og:title"]')
      .should('have.attr', 'content')
      .and('equal', 'Join Minds, and Elevate the Conversation');

    cy.get('head meta[property="og:type"]')
      .should('have.attr', 'content')
      .and('equal', 'website');

    cy.get('head meta[property="og:description"]')
      .should('have.attr', 'content')
      .and(
        'equal',
        'Minds is an open source social network dedicated to Internet freedom. Speak freely, protect your privacy, earn crypto rewards and take back control of your social media.'
      );

    cy.get('head meta[property="og:site_name"]')
      .should('have.attr', 'content')
      .and('equal', 'Minds');

    cy.get('head link[rel="canonical"]')
      .should('have.attr', 'href')
      .and('match', /\/register$/);

    cy.title().should('eq', 'Join Minds, and Elevate the Conversation');
  });

  it('should set correct metadata when a referrer param IS present', () => {
    cy.intercept('GET', '**/api/v1/channel/**').as('getChannel');

    cy.visit('/register?referrer=Minds').wait('@getChannel');

    cy.get('head meta[property="og:image"]')
      .should('have.attr', 'content')
      .and('match', /\/icon\/\d+\/large\/\d+/);

    cy.get('head meta[property="og:title"]')
      .should('have.attr', 'content')
      .and('equal', 'Join minds on Minds');

    cy.get('head meta[property="og:type"]')
      .should('have.attr', 'content')
      .and('equal', 'website');

    cy.get('head meta[property="og:description"]')
      .should('have.attr', 'content')
      .and(
        'equal',
        'Minds is an open source social network dedicated to Internet freedom. Speak freely, protect your privacy, earn crypto rewards and take back control of your social media.'
      );

    cy.get('head meta[property="og:site_name"]')
      .should('have.attr', 'content')
      .and('equal', 'Minds');

    cy.get('head link[rel="canonical"]')
      .should('have.attr', 'href')
      .and('match', /\/register$/);

    cy.title().should('eq', 'Join minds on Minds');
  });

  it.skip('should allow a user to register', () => {
    //type values
    cy.get(usernameField).focus().type(username);

    cy.get(emailField).focus().type(email);

    cy.get(passwordField).focus().type(password);

    cy.wait(500);

    cy.get(password2Field).focus().type(password);

    cy.get(checkbox).click({ force: true });

    //submit
    cy.get(submitButton)
      .contains('Join Now')
      .click()
      .wait('@register')
      .then((xhr) => {
        expect(xhr.response.statusCode).to.equal(200);
      });

    cy.get(mindsAvatar).should('be.visible');
  });
});
