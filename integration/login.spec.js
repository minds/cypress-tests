context('Login', () => {
  beforeEach(() => {
    cy.clearCookies();
    cy.visit('/').location('pathname').should('eq', `/`);

    cy.rateLimitBypass();
  });

  it('should login', () => {
    cy.get('.m-topbarLoginWrapper__login').contains('Login').click();

    // type username and password
    cy.get('[data-cy=data-minds-login-username-input]').type(
      Cypress.env().username
    );

    cy.get('[data-cy=data-minds-login-password-input]').type(
      Cypress.env().password
    );

    // click login button
    cy.get('[data-cy=data-minds-login-button]').click();

    cy.location('pathname').should('eq', '/newsfeed/subscriptions/latest');
  });

  it('should fail to login because of incorrect password', () => {
    cy.get('.m-topbarLoginWrapper__login').contains('Login').click();

    // cy.get('.m-login__wrapper').should('be.visible');

    // type username and password
    cy.get('[data-cy=data-minds-login-username-input]').type(
      Cypress.env().username
    );

    cy.get('[data-cy=data-minds-login-password-input]').type(
      Cypress.env().password + '1'
    );

    // click login button
    cy.get('[data-cy=data-minds-login-button]').click();

    cy.get('m-loginForm .m-error-box .mdl-card__supporting-text');
  });
});
