import generateRandomId from "../../support/utilities";

context('Search', () => {

  const searchText1 = 'minds';
  const searchText2 = 'c';
  const searchText3 = generateRandomId();
  const searchText4= '#art';
  const searchField = '[id="search"]';
  const searchSuggestions = '.m-searchBarSuggestions__suggestion';
  const searchResultsFeed = 'm-discovery__feedItem';
  const searchResultsChannel = '.m-publisherCardOwnerRow__owner';
  const discoveryTab = '.m-tabs__tab';
  const selectedTabOnDiscovery = '.m-tabs__tab--selected';
  const noResultsScroll = 'infinite-scroll';
  const discoveryTagWidget = '.m-discovery__tagWidget';

  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });
  });

  beforeEach(() => {
    cy.preserveCookies();

    cy.intercept('POST', '**/v2/newsfeed**').as('postActivity');
    cy.intercept('GET', '**/api/v2/search/suggest**').as('GETSuggest');

    cy.get(searchField)
      .should('be.visible');
  });

  it.skip('should display search suggestions on Newsfeed', () => {
    cy.get(searchField)
      .type(searchText1)
      .wait('@GETSuggest');

    cy.get(searchSuggestions)
      .contains('@minds')
      .should('be.visible');
    
    cy.get(searchField)
      .clear();
  });

  it.skip('should display maximum 10 search suggesstions on Newsfeed', () => {
    cy.get(searchField)
      .type(searchText2)
      .wait('@GETSuggest');

    cy.get(searchSuggestions)
      .should('have.length', 10);

    cy.get(searchField)
      .clear();
  });

  it.skip('should display search results on Discovery Top tab', () => {
    cy.get(searchField)
      .type(searchText1)
      .wait('@GETSuggest');
    
    cy.get(searchField)
      .type('{enter}')
      .location('href')
      .should(
        'contains',
        'discovery/search?q=minds&f=top&t=all'
      );

    cy.get(selectedTabOnDiscovery)
      .contains('Top')
      .should('be.visible');

    cy.get(searchResultsFeed)
      .first()
      .contains('minds')
      .should('be.visible');
  });

  it.skip('should display search results on Discovery Latest tab', () => {
    cy.get(discoveryTab)
      .contains('Latest')
      .click()
      .location('href')
      .should(
        'contains',
        'discovery/search?q=minds&f=latest&t=all'
      );

    cy.get(selectedTabOnDiscovery)
      .contains('Latest')
      .should('be.visible');

    cy.get(searchResultsFeed)
      .first()
      .contains('minds')
      .should('be.visible');
  });

  it.skip('should display search results on Discovery Channels tab', () => {
    cy.get(discoveryTab)
      .contains('Channels')
      .click()
      .location('href')
      .should(
        'contains',
        'discovery/search?q=minds&f=channels&t=all'
      );

    // Alternative to waiting for page to render - wait till you can see sub button.
    cy.get('m-subscribebutton').should('be.visible');

    cy.get(selectedTabOnDiscovery)
      .contains('Channels')
      .should('be.visible');

    cy.get(searchResultsChannel)
      .contains('@minds')
      .should('be.visible');

    cy.get(searchField)
      .clear();
  });

  it.skip('should display no results on Discovery Channels tab', () => {
    cy.get(searchField)
      .type(searchText3)
      .wait('@GETSuggest')
    
    cy.get(searchField)
      .type('{enter}')
      .location('href')
      .should(
        'contains',
        'discovery/search?q='+searchText3+'&f=channels&t=all'
      );

    cy.get(selectedTabOnDiscovery)
      .contains('Channels')
      .should('be.visible');

    cy.get(noResultsScroll)
      .contains('Nothing more to load')
      .should('be.visible');
    
    cy.get(searchField)
      .clear();
  });

  it.skip('should be able to search for tags in Discovery tab', () => {
    cy.get(searchField)
      .type(searchText4)
      .wait('@GETSuggest')
    
    cy.get(searchField)
      .type('{enter}');

    cy.get(discoveryTagWidget)
      .contains('add')
      .should('be.visible');

    cy.get(discoveryTagWidget)
      .contains('#art')
      .should('be.visible');

    cy.get(searchField)
      .clear();
  });
});
