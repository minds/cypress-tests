context('Discovery -> Discover by tags', () => {
  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (!sessionCookie) {
        return cy.login(true);
      }
    });
  });

  beforeEach(() => {
    cy.preserveCookies();

    cy.server();
    cy.route('GET', '**/api/v2/hashtags/suggested**').as('getTags');
    cy.route('POST', '**/api/v3/discovery/tags').as('postTags');
  });

  const discoverySettingsButton =
    '[data-cy="discover-by-tags-settings-button"]';

  const openSettingsModal = () => {
    cy.get(discoverySettingsButton)
      .should('be.visible')
      .click();
    return;
    
    // Wait for the tags to load
    return cy.wait('@getTags').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body.status).to.equal('success');

      return xhr.response.body;
    });
  };

  it('should navigate to discovery by tags', () => {
    cy.get('m-sidebar--navigation')
      .contains('Discovery')
      .click()
      .location('href')
      .should('contain', '/discovery/top');
  });

  it.skip('should open modal and select a tag', () => {
    openSettingsModal()
    cy.get('.m-modalV2__inner').should('be.visible');

    const firstTag = cy.get(
      '[data-cy=discovery-settings-section--other] li.m-discoveryTagSettingsList__item.ng-star-inserted'
    ).first();

    // first hover over
    firstTag.trigger('mouseover');
    // then click
    firstTag
      .find('i')
      .contains('add')
      .click({ force: true });
    
    cy.get('button.m-button.m-button--blue.m-button--medium div.m-button__text--unsaved.ng-star-inserted')
      .contains('Save')
      .should('not.be.disabled')
      .click();

    cy.wait('@postTags').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body.status).to.equal('success');
    });

    cy.get('.m-modalV2__inner').should('not.visible');
  });

  it.skip('should remove a tag', () => {
    openSettingsModal()
    const firstTag = cy.get(
      '[data-cy=discovery-settings-section--selected] li.m-discoveryTagSettingsList__item.ng-star-inserted'
    ).first();


    // first hover over
    firstTag.trigger('mouseover');
    // then click
    firstTag
      .find('i')
      .contains('remove')
      .click({ force: true });

    cy.get('button.m-button.m-button--blue.m-button--medium div.m-button__text--unsaved.ng-star-inserted')
      .contains('Save')
      .should('not.be.disabled')
      .click();

    cy.wait('@postTags').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body.status).to.equal('success');
    });

    cy.get('.m-modalV2__inner').should('not.visible');
  });

  it.skip('should add a manual tag', () => {
    openSettingsModal().then(({ tags, trending }) => {
      // Wait until lenght is resolved
      cy.get(
        `[data-cy="discovery-settings-section--selected"] > ul > li`
      ).should('have.length', tags.length);

      const time = Date.now();
      cy.get('[data-cy="discovery-settings-input"]').type(
        `tmptag${time}{enter}`
      );

      cy.get(
        `[data-cy="discovery-settings-section--selected"] > ul > li`
      ).should('have.length', tags.length + 1);

      cy.reload();
    });
  });
});
