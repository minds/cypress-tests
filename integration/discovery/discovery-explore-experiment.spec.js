context('Discovery Explore Experiment', () => {
  before(() => {
    cy.login(true);
  });

  beforeEach(() => {
    cy.viewport(1920, 1080);
    cy.preserveCookies();
  });

  it('should test with explore flag on', () => {
    cy.forceExperimentVariations({
      'minds-3038-discovery-explore': true,
    });
    cy.visit('/');
    cy.get('[data-ref=sidenav-discovery] span').contains('Explore');
  });

  it.skip('should test with explore flag off', () => {
    cy.forceExperimentVariations({
      'minds-3038-discovery-explore': false,
    });
    cy.visit('/');
    cy.get('[data-ref=sidenav-discovery] span').contains('Discovery');
  });
});
