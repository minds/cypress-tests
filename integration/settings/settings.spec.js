context('Settings', () => {

  const settingsHeader = 'div.m-nestedMenu__headerLabel';
  const settingsTopMenus = 'div.m-nestedMenu__menuWrapper.ng-star-inserted';
  const settingsSubMenusHeading = 'a.m-nestedMenu__itemWrapper';
  const settingsSubMenusChildren = 'a.m-nestedMenu__itemWrapper.ng-star-inserted';

  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });
  });

  beforeEach(() => {
    cy.preserveCookies();
    cy.visit('/settings');
    cy.get(settingsHeader).should('be.visible');
  });

  it('should load all Top nested menus within settings', () => {
    cy.get(settingsTopMenus)
      .children()
      .should('contain', 'Account')
      .and('contain', 'Pro')
      .and('contain', 'Security')
      .and('contain', 'Billing')
      .and('contain', 'Other');
  });

  it('should load all Accounts submenus within settings', () => {
    cy.get(settingsSubMenusHeading)
      .contains('Account')
      .click();
    
    cy.get(settingsHeader).should('be.visible');

    cy.get(settingsSubMenusChildren)
      .should('contain', 'Profile')
      .and('contain', 'Email Address')
      .and('contain', 'Language')
      .and('contain', 'Password')
      .and('contain', 'Boosted Content')
      .and('contain', 'NSFW Content')
      .and('contain', 'Share Buttons')
      .and('contain', 'Autoplay Videos')
      .and('contain', 'Messenger')
      .and('contain', 'Push Notifications')
      .and('contain', 'Email Notifications')
      .and('contain', 'Upgrade to Pro')
      .and('contain', 'Upgrade to Plus');
  });

  it('should load all Pro submenus within settings', () => {
    cy.get(settingsSubMenusHeading)
      .contains('Pro')
      .click();
    
    cy.get(settingsHeader).should('be.visible');

    cy.get(settingsSubMenusChildren)
      .should('contain', 'General')
      .and('contain', 'Theme')
      .and('contain', 'Assets')
      .and('contain', 'Hashtags')
      .and('contain', 'Footer')
      .and('contain', 'Domain')
      .and('contain', 'Payouts')
      .and('contain', 'Cancel Pro Subscription')
      .and('contain', 'View Pro Channel');
  });

  it('should load all Security submenus within settings', () => {
    cy.get(settingsSubMenusHeading)
      .contains('Security')
      .click();
    
    cy.get(settingsHeader).should('be.visible');

    cy.get(settingsSubMenusChildren)
      .should('contain', 'Two-factor Authentication')
      .and('contain', 'Sessions');
  });

  it('should load all Billing submenus within settings', () => {
    cy.get(settingsSubMenusHeading)
      .contains('Billing')
      .click();
    
    cy.get(settingsHeader).should('be.visible');

    cy.get(settingsSubMenusChildren)
      .should('contain', 'Payment Methods')
      .and('contain', 'Recurring Payments');
  });

  it('should load all Other submenus within settings', () => {
    cy.get(settingsSubMenusHeading)
      .contains('Other')
      .click();
    
    cy.get(settingsHeader).should('be.visible');

    cy.get(settingsSubMenusChildren)
      .should('contain', 'Referrals')
      .and('contain', 'Wallet')
      .and('contain', 'Reported Content')
      .and('contain', 'Blocked Channels')
      .and('contain', 'Subscription Tier Management')
      .and('contain', 'Twitter')
      .and('contain', 'Youtube')
      .and('contain', 'Deactivate Account')
      .and('contain', 'Delete Account');
  });
});
